$(function () {

    $("#editForm input,#editForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function ($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function ($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var id = $("input#id").val();
            var title = $("input#title").val();
            var body = $("textarea#body").val();
            var image = $("input#image").val();
            var source = $("input#source").val();
            var publisher = $("input#publisher").val();
            $this = $("#sendMessageButton");
            $this.prop("disabled", true); // Disable submit button until AJAX call is complete to prevent duplicate messages
            $.ajax({
                url: base_url+"feed/update",
                type: "POST",
                data: {
                    id: id,
                    title: title,
                    image: image,
                    body: body,
                    publisher: publisher,
                    source: source
                },
                cache: false,
                success: function () {
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                    $('#success > .alert-success')
                            .append("<strong>Información guardada correctamente. </strong>");
                    $('#success > .alert-success')
                            .append('</div>');
                },
                error: function () {
                    // Fail message
                    $('#success').html("<div class='alert alert-danger'>");
                    $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                    $('#success > .alert-danger').append($("<strong>").text("Ha habido un error guardando la información. Revisa los campos."));
                    $('#success > .alert-danger').append('</div>');
                },
                complete: function () {
                    setTimeout(function () {
                        $this.prop("disabled", false); // Re-enable submit button when AJAX call is complete
                    }, 1000);
                }
            });
        },
        filter: function () {
            return $(this).is(":visible");
        }
    });

    $("a[data-toggle=\"tab\"]").click(function (e) {
        e.preventDefault();
        $(this).tab("show");
    });
    
    $(".btn-delete").click(function(){
        var id = $("input#id").val();
        $.ajax({
            url: base_url+"feed/delete",
            type: "POST",
            data: {
                id: id
            },
            cache: false,
            success: function () {
                // Success message
                $('#success').html("<div class='alert alert-success'>");
                $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                $('#success > .alert-success')
                        .append("<strong>Feed eliminado correctamente.</strong>");
                $('#success > .alert-success')
                        .append('</div>');
                setTimeout(function () {
                    window.location.replace(base_url);
                }, 2000);
            },
            error: function () {
                // Fail message
                $('#success').html("<div class='alert alert-danger'>");
                $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                $('#success > .alert-danger').append($("<strong>").text("Ha habido un error eliminando el feed."));
                $('#success > .alert-danger').append('</div>');
            },
            complete: function () {
                setTimeout(function () {
                    $this.prop("disabled", false); // Re-enable submit button when AJAX call is complete
                }, 1000);
            }
        });
    });
});

/*When clicking on Full hide fail/success boxes */
$('#title').focus(function () {
    $('#success').html('');
});
