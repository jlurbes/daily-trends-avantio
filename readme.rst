###################
DAILY TRENDS
###################

Aplicación creada como prueba para Avantio

*******************
Instrucciones de instalación
*******************

1. Clonar el repositorio: git clone https://jlurbes@bitbucket.org/jlurbes/daily-trends-avantio.git
2. Copiar en la carpeta htdocs de un entorno XAMPP o en la carpeta correspondiente del servidor.
3. La aplicación está preparada para trabajar con un mysql configurado con user "root" y contraseña "". De querer cambiar esta configuración acceder al archivo "application/config/database.php" donde se podrá cambiar está configuración por defecto.
4. En el navegador introducir la url http://localhost/{nombre_de_la_carpeta_utilizada} o la url correspondiente según el servidor utilizado.
5. La aplicación debería arrancar. La primera vez, se crea la base de datos junto con la tabla de gestión de feeds. Según la carga de los periódicos esa primera carga de información puede durar varios segundos.
6. El dibujo con la arquitectura de la aplicación se puede encontrar en la raíz del proyecto con nombre: arquitectura.png