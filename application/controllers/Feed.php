<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Feed extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Feed_model');
    }

    public function index() {
        $feeds['feeds']  = $this->Feed_model->get_feeds();
        // Si tenemos feeds, primero comprobamos de qué día son.
        $feeds_loaded = false;
        if (!empty($feeds['feeds'])) {
            $feeds_loaded = $this->checkFeedDate($feeds['feeds']['0']['date']);
            // Si no son de hoy, los borramos
            if ($feeds_loaded === false) {
                $this->Feed_model->delete_feeds();
            }
        }
        if ($feeds_loaded === false) {
            $feeds['feeds'] = $this->getNewFeeds();
        }
        $this->load->view('feed', $feeds);
    }
    
    public function create(){
        $this->load->view('new');
    }
    
    public function edit($id){
        if(!empty($id)){
            $feed['feed'] = $this->Feed_model->get_feed($id);
        }
        $this->load->view('edit', $feed);
    }
    
    public function save(){
        
        $title = $this->input->post('title');
        $body = $this->input->post('body');
        $image = $this->input->post('image');
        $source = $this->input->post('source');
        $publisher = $this->input->post('publisher');
        
        $res = $this->Feed_model->new_feed($title, $body, $image, $source, $publisher);
        
        return $res;
    }
    
    public function update(){
        
        $id = $this->input->post('id');
        $title = $this->input->post('title');
        $body = $this->input->post('body');
        $image = $this->input->post('image');
        $source = $this->input->post('source');
        $publisher = $this->input->post('publisher');
        
        $res = $this->Feed_model->update_feed($id, $title, $body, $image, $source, $publisher);
        
        return $res;
    }
    
    public function delete(){
        $id = $this->input->post('id');
        if(!empty($id)){
            $res = $this->Feed_model->delete_feed($id);
        }
        return $res;
    }

    private function getNewFeeds() {
        // Feed El Mundo
        $feed_elmundo = $this->web_scraping_elmundo();
        // Feed El País
        $feed_elpais = $this->web_scraping_elpais();
        // Combinamos los resultados de los feeds
        $feeds = array_merge($feed_elmundo, $feed_elpais);

        $i = 0;
        $number_feeds = count($feeds);
        for ($i = 0; $i < $number_feeds; $i++) {
            // Insertamos los feed en la base de datos
            $id = $this->Feed_model->new_feed($feeds[$i]['title'], $feeds[$i]['body'], $feeds[$i]['image'], $feeds[$i]['source'], $feeds[$i]['publisher']);
            if($id !== false)
                $feeds[$i]['id'] = $id;
        }
        return $feeds;
    }

    /**
     * Función que devuelve true si el día de la fecha pasada es igual a la de hoy. 
     * De lo contrario, devuelve false.
     * 
     * @param type $date
     * @return boolean
     */
    private function checkFeedDate($date) {
        if (!empty($date)) {
            if (date('Y-m-d', strtotime($date)) === date('Y-m-d')) {
                return true;
            }
        }
        return false;
    }

    /**
     * Función para obtener la información de la noticia de portada de El Mundo
     */
    private function web_scraping_elmundo() {
        $url = 'http://www.elmundo.es';
        $html = file_get_html($url);
        // Nos quedamos con las noticias de portadas        
        for ($i = 0; $i < ELMUNDO_MAX_NEWS; $i++) {
            $article_url = $html->find('.content-item article h2 a', $i)->href;
            // var_dump($article_url);
            $feed[] = $this->getInfoNewsElMundo($article_url);
        }
        $html->clear();
        return $feed;
    }

    private function getInfoNewsElMundo($url) {
        $html = file_get_html($url);
        $feed = array();
        // Obtenemos el título;
        $feed['title'] = $html->find('article .titles h1', 0)->text();
        // Obtenemos la descripción. Sólo el primer párrafo
        $feed['body'] = $html->find('article .content p', 3)->text();
        // Obtenemos la imagen
        $feed['image'] = $html->find('article figure img', 0)->src;
        // Obtenemos la fuente        
        $feed['source'] = '';
        $author_twitter = $html->find('.news-author .author-twitter', 0);
        if (!empty($author_twitter))
            $feed['source'] = $author_twitter->text();
        else {
            $authors = $html->find('.news-author .author-name');
            $i = 0;
            foreach ($authors as $author) {
                if ($i > 0) {
                    $feed['source'] .= " | ";
                }
                $feed['source'] .= $author->text();
                $i++;
            }
            $author_city = $html->find('.news-author .author-city', 0);
            if (!empty($author_city)) {
                $feed['source'] .= " |";
                $feed['source'] .= $author_city->text();
            }
        }
        // Seteamos el periódico de publicación.
        $feed['publisher'] = 'El Mundo';
        return $feed;
    }

    /**
     * Función para obtener la información de la noticia de portada de El País
     */
    private function web_scraping_elpais() {
        $url = 'https://elpais.com';
        $html = file_get_html($url);
        // Nos quedamos con las noticias de portadas        
        for ($i = 0; $i < ELPAIS_MAX_NEWS; $i++) {
            $article_url = $html->find('article h2.articulo-titulo a', $i)->href;
            $feed[] = $this->getInfoNewsElPais($article_url);
        }
        $html->clear();
        return $feed;
    }

    private function getInfoNewsElPais($url) {
        $html = file_get_html($url);
        $feed = array();
        // Obtenemos el título;
        $feed['title'] = $html->find('article h1.articulo-titulo', 0)->text();
        // Obtenemos la descripción. Sólo el primer párrafo
        $feed['body'] = $html->find('article .articulo__contenedor p', 0)->text() . "...";
        // Obtenemos la imagen
        $image = $html->find('article #videonoticia figure meta',0);
        if(!empty($image)){
            $feed['image'] = $image->getAttribute('content');
        }
        else{
            $feed['image'] = $html->find('article .articulo__contenedor img', 0)->getAttribute('data-src');
        }
        // Obtenemos la fuente        
        $feed['source'] = '';
        $authors = $html->find('article .articulo-apertura .autor-nombre');
        $i = 0;
        foreach($authors as $auth){
            $author = $auth->find('a',0);
            if (!empty($author)){
                if($i>0)
                    $feed['source'] .= " | ";
                $feed['source'] .= $author->text();
            }
            $i++;
        }
        // Seteamos el periódico de publicación.
        $feed['publisher'] = 'El País';
        return $feed;
    }

}
