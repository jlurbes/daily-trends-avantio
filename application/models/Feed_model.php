<?php

class Feed_model extends CI_Model {

    public $title;
    public $body;
    public $image;
    public $source;
    public $publisher;

    // Getters
    function getTitle() {
        return $this->title;
    }

    function getBody() {
        return $this->body;
    }

    function getImage() {
        return $this->image;
    }

    function getSource() {
        return $this->source;
    }

    function getPublisher() {
        return $this->publisher;
    }

    // Setters
    function setTitle($title) {
        $this->title = $title;
    }

    function setBody($body) {
        $this->body = $body;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setSource($source) {
        $this->source = $source;
    }

    function setPublisher($publisher) {
        $this->publisher = $publisher;
    }

    public function get_feeds() {
        $this->db->from('feeds');
        $this->db->order_by('date DESC, id ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_feed($id) {
        $query = $this->db->get_where('feeds', array('id' => $id));
        return $query->row_array();
    }

    public function delete_feeds() {
        $query = $this->db->truncate('feeds');
        return $query;
    }

    public function delete_feed($id) {
        $query = $this->db->delete('feeds', array('id' => $id));
        return $query;
    }

    public function new_feed($title, $body, $image, $source, $publisher) {
        // Setting values
        $this->setTitle($title);
        $this->setBody($body);
        $this->setImage($image);
        $this->setSource($source);
        $this->setPublisher($publisher);
        // Insert new feed on database
        $ok = $this->db->insert('feeds', $this);
        // Everything fine -> $ok = true. If not $ok = false
        if ($ok) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function update_feed($id, $title, $body, $image, $source, $publisher) {
        // Setting values        
        $this->setTitle($title);
        $this->setBody($body);
        $this->setImage($image);
        $this->setSource($source);
        $this->setPublisher($publisher);
        // Update existing feed on database
        $this->db->where('id', $id);
        $ok = $this->db->update('feeds', $this, array('id' => $id));
        // Everything fine -> $ok = true. If not $ok = false
        return $ok;
    }

}
